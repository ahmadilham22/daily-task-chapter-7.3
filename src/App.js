import { useState } from "react"
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  const [count, setCount] = useState(0);

  const decCount = () => {
    setCount(prevCount => prevCount - 1)
  }

  const inCount = () => {
    setCount(prevCount => prevCount + 1)
  }

  const [name, setName] = useState("Unknown")

  const firstName = () => {
    setName("Ahmad Ilham")
  }

  const secondName = () => {
    setName("Imam Hermawan")
  }

  return (
    <div className="App">
      <header className="App-header"> 
        <div className="row">
        <div className="col-12 mt-5">
              <h3>Assalamu'alaikum, {name}</h3>
              <button className="btn btn-primary m-3" onClick={firstName}>Ilham</button>
              <button className="btn btn-primary m-3" onClick={secondName}>Imam</button>
          </div>
          <div className="col-12 mb-5">
              <h2>Total Click</h2>
              <button className="btn btn-primary m-3" onClick={decCount}>-</button>
              <h4>{count}</h4>
              <button className="btn btn-primary m-3" onClick={inCount}>+</button>
          </div>
          
        </div>
      </header>
    </div>
  );
}

export default App;
